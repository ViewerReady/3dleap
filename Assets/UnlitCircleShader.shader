﻿Shader "Unlit/UnlitCircleShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CenterL("LeftCenter", Vector) = (.75,.5,0,0)
		_CenterR("RightCenter", Vector) = (.25,.5,0,0)

		_Radius("Radius", Float) = 1.0
		
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Radius;
			float2 _CenterR;
			float2 _CenterL;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float2 mod = float2(i.uv.x, .5 + (i.uv.y -.5)/2);
				clip(distance(mod, _CenterL)<_Radius || distance(mod, _CenterR)<_Radius ? 1 : -1);
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}

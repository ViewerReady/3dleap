﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardToCamera : MonoBehaviour {
    public Transform cam;
	public bool useTargetUp;
    public bool lock_x;
	// Update is called once per frame
	void Update () {
        Transform target = cam != null ? cam : Camera.main.transform;
        Quaternion look = Quaternion.LookRotation(target.position - this.transform.position, useTargetUp ? target.up : Vector3.up);
        if (lock_x)
        {
            Vector3 euler = look.eulerAngles;
            euler.x = 0;
            look.eulerAngles = euler;
        }
        transform.rotation = look;
	}
}

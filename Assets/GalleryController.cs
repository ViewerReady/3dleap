﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryController : MonoBehaviour {
    public GameObject[] exhibits;
    public GameObject cameraFlash;
    public float exhibitDistance = 1.5f;
    private int currentExhibit = -1;
	// Use this for initialization
	void Start () {
        RefreshExhibits();
	}

    public void CameraFlash()
    {
        StartCoroutine(CameraFlashRoutine());
    }

    IEnumerator CameraFlashRoutine()
    {
        cameraFlash.SetActive(false);
        yield return null;
        cameraFlash.SetActive(true);
    }

    public void DisplayNextExhibit()
    {
        currentExhibit++;
        if(currentExhibit>=exhibits.Length || currentExhibit<0)
        {
            currentExhibit = 0;
        }
        RefreshExhibits();
    }
    
    public void RefreshExhibits()
    {
        for(int e = 0; e<exhibits.Length; e++)
        {
            exhibits[e].SetActive(e == currentExhibit);
            if(e==currentExhibit)
            {
                exhibits[e].SetActive(true);
                Vector3 flatLookDirection = Camera.main.transform.forward;
                flatLookDirection.y = 0f;
                if(flatLookDirection != Vector3.zero)
                {
                    exhibits[e].transform.position = Camera.main.transform.position + flatLookDirection.normalized * exhibitDistance;
                }
            }
            else
            {
                exhibits[e].SetActive(false);
            }
        }
    }
}

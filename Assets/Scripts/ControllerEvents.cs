﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.MagicLeap;

public class ControllerEvents : MonoBehaviour {
    public UnityEvent OnShoulderButtonDown;
    public UnityEvent OnShoulderButtonUp;
    public UnityEvent OnHomeButtonDown;
    public UnityEvent OnHomeButtonUp;

	// Use this for initialization
	void Awake () {
        MLResult result = MLInput.Start();
        if (!result.IsOk)
        {
            Debug.LogError("Error RaycastExample starting MLInput, disabling script.");
            enabled = false;
            return;
        }

        MLInput.OnControllerButtonDown += OnButtonDown;
        MLInput.OnControllerButtonUp += OnButtonUp;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    #region Event Handlers
    
    private void OnButtonDown(byte controller_id, MLInputControllerButton button)
    {
        switch(button)
        {
            case MLInputControllerButton.Bumper:
                OnShoulderButtonDown.Invoke();
                break;
            case MLInputControllerButton.HomeTap:
                OnHomeButtonDown.Invoke();
                break;
        }
    }

    private void OnButtonUp(byte controller_id, MLInputControllerButton button)
    {
        switch (button)
        {
            case MLInputControllerButton.Bumper:
                OnShoulderButtonUp.Invoke();
                break;
            case MLInputControllerButton.HomeTap:
                OnHomeButtonUp.Invoke();
                break;
        }
    }

    #endregion
}
